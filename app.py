from flask import Flask, render_template, request, jsonify

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        try:
            temperature = float(request.form['temperature'])
            unit_from = request.form['unit_from']
            unit_to = request.form['unit_to']
            converted_temperature = convert_temperature(temperature, unit_from, unit_to)
            return render_template('converter.html', result=converted_temperature)
        except ValueError:
            return render_template('converter.html', error="Invalid input")

    return render_template('converter.html')

def convert_temperature(temperature, unit_from, unit_to):
    if unit_from == 'Fahrenheit' and unit_to == 'Celsius':
        return (temperature - 32) * 5/9
    if unit_from == 'Celsius' and unit_to == 'Fahrenheit':
        return (temperature * 9/5) + 32
    if unit_from == 'Fahrenheit' and unit_to == 'Fahrenheit':
        return temperature
    if unit_from == 'Celsius' and unit_to == 'Celsius':
        return temperature

if __name__ == '__main__':
    app.run(host='0.0.0.0')
